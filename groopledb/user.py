# Copyright 2017 Jacques Supcik <jacques@supcik.net>
#                Passeport vacances Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#############################################################################
#   ____ _       _   _             ____            _           _
#  / ___| | ___ | |_| |__   ___   |  _ \ _ __ ___ (_) ___  ___| |_
# | |   | |/ _ \| __| '_ \ / _ \  | |_) | '__/ _ \| |/ _ \/ __| __|
# | |___| | (_) | |_| | | | (_) | |  __/| | | (_) | |  __/ (__| |_
#  \____|_|\___/ \__|_| |_|\___/  |_|   |_|  \___// |\___|\___|\__|
#                                               |__/
#############################################################################


class User:
    dir = dict()

    def __init__(self, userid, firstname, lastname, street, city, gender, age, twinning):
        self.userid = userid
        self.firstname = firstname
        self.lastname = lastname
        self.street = street
        self.city = city
        self.gender = gender
        self.age = age
        self.twinning = twinning
        self.available_periods = set()

        self.activity_wanted = list()
        self.activity_received = set()
        self.occurrence_received = set()
        self.occurrence_chosen_after_attribution = set()
        self.covered_periods = set()

        self.attributes = dict()

        self.register()

    def to_dict(self):
        return {
            "firstname": self.firstname,
            "lastname": self.lastname,
            "street": self.street,
            "city": self.city,
            "age": self.age,
            "gender": self.gender,
            "twinning": self.twinning,
            "available_periods": sorted([i.perid for i in list(self.available_periods)]),
            "activity_wanted": [i.actid for i in self.activity_wanted],
            "activity_received": sorted([i.actid for i in list(self.activity_received)]),
            "groups_attributed": sorted([i.ocid for i in list(self.occurrence_received)]),
            "groups_chosen_after_attribution": sorted([i.ocid for i in list(self.occurrence_chosen_after_attribution)]),
            "covered_periods": sorted([i.perid for i in list(self.covered_periods)]),
        }

    def register(self):
        User.dir[self.userid] = self

    def unregister(self):
        del User.dir[self.userid]

    @staticmethod
    def get(userid):
        return User.dir.get(userid, None)
