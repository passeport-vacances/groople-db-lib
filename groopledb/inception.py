# Copyright 2017 Jacques Supcik <jacques@supcik.net>
#                Passeport vacances Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#############################################################################
#   ____ _       _   _             ____            _           _
#  / ___| | ___ | |_| |__   ___   |  _ \ _ __ ___ (_) ___  ___| |_
# | |   | |/ _ \| __| '_ \ / _ \  | |_) | '__/ _ \| |/ _ \/ __| __|
# | |___| | (_) | |_| | | | (_) | |  __/| | | (_) | |  __/ (__| |_
#  \____|_|\___/ \__|_| |_|\___/  |_|   |_|  \___// |\___|\___|\__|
#                                               |__/
#############################################################################

import datetime
import glob
import hmac
import json
import logging
import os
import re

from . import Activity, Occurrence, Period, User


def read_direct_dump(directory, password=None):
    logger = logging.getLogger(__name__)
    logger.debug("Reading from json files")
    if password is not None:
        logger.debug("Data will be anonymized")
        hmac_key = bytearray(password, encoding="utf-8")
    else:
        logger.debug("WARNING : Data will NOT be anonymized")

    def anon(s: str):
        if password is not None:
            hm = hmac.new(hmac_key, digestmod="sha256")
            hm.update(bytearray(s, encoding="utf-8"))
            return hm.hexdigest()
        else:
            return s

    # Read all json files
    data = dict()
    for f in glob.glob(os.path.join(directory, "*.json")):
        base, _ = os.path.splitext(os.path.basename(f))
        data[base] = json.load(open(f, encoding="utf-8"))

    street_field = "attribute_2"
    city_field = "attribute_3"
    gender_field = "attribute_4"
    age_field = "attribute_5"
    twinning_field = None

    user_attribute_by_id = dict()
    for this in data["_user_attributes"]:
        user_attribute_by_id[this["attribute_id"]] = this["attribute_label"]
        if re.match(r'Route et numéro', this["attribute_label"]):
            street_field = this["attribute_name"]
            logger.debug("Found street attribute as {0}".format(street_field))
        elif re.match(r'NPA Lieu', this["attribute_label"]):
            city_field = this["attribute_name"]
            logger.debug("Found city attribute as {0}".format(city_field))
        elif re.match(r'Fille ou garçon', this["attribute_label"]):
            gender_field = this["attribute_name"]
            logger.debug("Found gender attribute as {0}".format(gender_field))
        elif re.match(r'[âÂ]ge révolu', this["attribute_label"]):
            age_field = this["attribute_name"]
            logger.debug("Found age attribute as {0}".format(age_field))
        elif re.match(r'Jumelage souhaité avec', this["attribute_label"]):
            twinning_field = this["attribute_name"]
            logger.debug("Found twinning attribute as {0}".format(twinning_field))

    for this in data["activities"]:
        a = Activity(this["activity_id"], this["activity_label"], this["category_label"])
        a.description = this["information"]

    for this in data["activities_attributes"]:
        a = Activity.get(this["activity_id"])
        a.attributes[this["attribute_label"]] = this["value"]

    for this in data["activities_users_attributes_values"]:
        a = Activity.get(this["activity_id"])
        key = user_attribute_by_id[this["user_attribute_id"]]
        a.user_attributes[key] = this["attribute_value"]
        if re.match(r'[AÂaâ]ge r[eé]volu', key):
            Activity.get(this["activity_id"]).required_ages.add(int(this["attribute_value"]))

    for this in data["groups"]:
        o = Occurrence(this["group_id"], this["group_label"], this["minQuota"], this["maxQuota"])
        a = Activity.get(this["activity_id"])
        o.activity = a
        a.add_occurence(o)

    for this in data["groups_attributes"]:
        o = Occurrence.get(this["group_id"])
        key = this["attribute_label"]
        val = this["value"]
        o.attributes[key] = val

        if re.match(r'Horaire', key):
            o.time = this["value"]
        elif re.match(r'Nombre minimum', key):
            o.minx = this["value"]

    for this in data["periods"]:
        Period(
            this["period_id"],
            this["period_name"],
            this["label"],
            this["parent_period_id"],
            this["parent_period_label"]
        )

    for this in data["groups_periods"]:
        p = Period.get(this["period_id"])
        o = Occurrence.get(this["group_id"])
        p.occurrences.add(o)
        o.periods.add(p)

    for this in data["users_with_attributes"]:
        User(
            this["user_id"],
            anon(this["firstname"]),
            anon(this["lastname"]),
            anon(this[street_field]),
            anon(this[city_field]),
            this[gender_field],
            this[age_field],
            this.get(twinning_field, "") if twinning_field is not None else "",
        )

    for this in data["users_attributes"]:
        u = User.get(this["user_id"])
        u.attributes[this["attribute_label"]] = this["value"]

    for this in data["users_availability"]:
        if this["availability"] == "available":
            u = User.get(this["user_id"])
            p = Period.get(this["period_id"])
            u.available_periods.add(p)

    for this in sorted(data["choices"], key=lambda x: x["id"]):
        u = User.get(this["user_id"])  # type: User
        a = Activity.get(this["activity_id"])
        u.activity_wanted.append(a)
        if this["hasBeenSelected"] != 0:
            u.activity_received.add(a)

    for this in data["attributions"]:
        u = User.get(this["user_id"])  # type: User
        o = Occurrence.get(this["group_id"])  # type: Occurrence
        o.participants.add(u)
        if this["hasBeenChosen"] == 1:
            u.occurrence_received.add(o)
        else:
            u.occurrence_chosen_after_attribution.add(o)

    for u in User.dir.values():  # type: User
        for o in u.occurrence_received | u.occurrence_chosen_after_attribution:
            u.covered_periods |= o.periods

    logger.debug("Done")


def read_summary(filename, password=None):
    if password is not None:
        hmac_key = bytearray(password, encoding="utf-8")

    def anon(s: str):
        if password is not None:
            hm = hmac.new(hmac_key, digestmod="sha256")
            hm.update(bytearray(s, encoding="utf-8"))
            return hm.hexdigest()
        else:
            return s

    data = json.load(open(filename))

    for k, v in data["activities"].items():
        a = Activity(k, v["title"], v["category"])
        a.required_ages = set(v["ages"])

    for k, v in data["groups"].items():
        o = Occurrence(k, v["label"], v["min"], v["max"])
        o.time = v["time"]
        o.minx = v["minx"]
        a = Activity.get(str(v["activity"]))
        o.activity = a
        a.add_occurence(o)

    for k, v in data["periods"].items():
        p = Period(k, v["name"], v["label"], v["parent_id"], v["parent_label"])
        for i in v["groups"]:
            o = Occurrence.get(str(i))
            p.occurrences.add(o)
            o.periods.add(p)

    for k, v in data["users"].items():
        u = User(
            k, anon(v["firstname"]), anon(v["lastname"]), anon(v["street"]),
            anon(v["city"]), v["gender"], v["age"], v["twinning"]
        )
        for i in v["available_periods"]:
            p = Period.get(str(i))
            u.available_periods.add(p)

        for i in v["activity_wanted"]:
            a = Activity.get(str(i))
            u.activity_wanted.append(a)

        for i in v["activity_received"]:
            a = Activity.get(str(i))
            u.activity_received.add(a)

        for i in v["groups_attributed"]:
            o = Occurrence.get(str(i))
            u.occurrence_received.add(o)

        for i in v["groups_chosen_after_attribution"]:
            o = Occurrence.get(str(i))
            u.occurrence_chosen_after_attribution.add(o)

        for i in v["covered_periods"]:
            p = Period.get(str(i))
            u.covered_periods.add(p)


def cleanup(year):
    def date_fmt(x, year):
        m = re.match(r"groupe\s+\d+", x, re.IGNORECASE)
        if m:
            return ""

        m = re.match(r"Tout le passeport", x, re.IGNORECASE)
        if m:
            return ""

        m = re.match(r"(\D+)(\d+)(\D+)(\d+)", x)
        if m:
            day = int(m.group(4))
            week = int(m.group(2))
            if week >= 3 and day < 15:
                month = 8
            else:
                month = 7
            # SPECIAL EXCEPTION DUE TO BUG IN Soufflage de verre 2017
            if day == 283:
                day = 28
            d = datetime.date(year, month, day)
            return d

        else:
            raise Exception("Bad Date : {0}".format(x))

    def time_range(x):
        if x == "":
            return None

        m = re.match(r"(?:de)?\s*(\d+)[h.](\d*) [à-] (\d+)[h.](\d*)", x)
        if m:
            h1 = int(m.group(1))
            m1 = int(m.group(2)) if len(m.group(2)) > 0 else 0
            h2 = int(m.group(3))
            m2 = int(m.group(4)) if len(m.group(4)) > 0 else 0
            return datetime.time(h1, m1), datetime.time(h2, m2)
        else:
            raise Exception("Bad time : {0}".format(x))

    for v in Period.dir.values():  # type: Period
        m = re.search(r"(\d{1,2})\.(\d{1,2})", v.label)
        if m:
            v.day = datetime.date(year, int(m.group(2)), int(m.group(1)))
        else:
            raise Exception("Bad day {0}".format(v["label"]))

    for v in Occurrence.dir.values():  # type: Occurence
        if v.time is not None:
            v.time = time_range(v.time)
        v.date = date_fmt(v.label, year)
        days = set()
        for p in v.periods:  # type: Period
            days.add(p.day)
        v.days = sorted(list(days), key=lambda t: t.toordinal())
