# Copyright 2017 Jacques Supcik <jacques@supcik.net>
#                Passeport vacances Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#############################################################################
#   ____ _       _   _             ____            _           _
#  / ___| | ___ | |_| |__   ___   |  _ \ _ __ ___ (_) ___  ___| |_
# | |   | |/ _ \| __| '_ \ / _ \  | |_) | '__/ _ \| |/ _ \/ __| __|
# | |___| | (_) | |_| | | | (_) | |  __/| | | (_) | |  __/ (__| |_
#  \____|_|\___/ \__|_| |_|\___/  |_|   |_|  \___// |\___|\___|\__|
#                                               |__/
#############################################################################

from .activity import Activity
from .user import User
from .period import Period
from .occurrence import Occurrence

import logging

logging.getLogger(__name__).addHandler(logging.NullHandler())
