# Copyright 2017 Jacques Supcik <jacques@supcik.net>
#                Passeport vacances Fribourg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#############################################################################
#   ____ _       _   _             ____            _           _
#  / ___| | ___ | |_| |__   ___   |  _ \ _ __ ___ (_) ___  ___| |_
# | |   | |/ _ \| __| '_ \ / _ \  | |_) | '__/ _ \| |/ _ \/ __| __|
# | |___| | (_) | |_| | | | (_) | |  __/| | | (_) | |  __/ (__| |_
#  \____|_|\___/ \__|_| |_|\___/  |_|   |_|  \___// |\___|\___|\__|
#                                               |__/
#############################################################################


class Occurrence:
    dir = dict()

    def __init__(self, ocid, label, mmin, mmax):
        self.ocid = ocid
        self.label = label
        self.min = mmin
        self.max = mmax
        self.periods = set()
        self.activity = None
        self.time = ""  # string / tupple of datetime.time
        self.minx = 0
        self.date = None  # from label, added by cleanup (datetime.date)
        self.days = None  # from periods, dded by cleanup (array of datetime.date)
        self.participants = set()

        self.attributes = dict()

        self.register()

    def to_dict(self):
        return {
            "label": self.label,
            "min": self.min,
            "minx": self.minx,
            "max": self.max,
            "time": self.time,
            "activity": self.activity.actid if self.activity is not None else "",
            "periods": sorted([i.perid for i in list(self.periods)]),
        }

    def register(self):
        Occurrence.dir[self.ocid] = self

    def unregister(self):
        del Occurrence.dir[self.ocid]

    @staticmethod
    def get(ocid):
        return Occurrence.dir.get(ocid, None)
