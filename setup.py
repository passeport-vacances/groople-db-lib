# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='groopledb',
    version='0.1.0',
    description='Library for handling Groople objects',
    long_description=readme,
    author='Jacques Supcik',
    author_email='jacques@supcik.net',
    url='https://gitlab.com/passeport-vacances/groopledb',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
